FROM balenalib/armv7hf-debian

RUN [ "cross-build-start" ]

# copy the dependencies file to the working directory
COPY requirements.txt /

RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential python3.9-minimal python3-pip \
    openssh-client libffi-dev python3-dev \
    rustc cargo libssl-dev libxml2-dev libxslt1-dev zlib1g-dev

ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

RUN export CRYPTOGRAPHY_DONT_BUILD_RUST=1 && \
    pip install cryptography==3.4.6
RUN pip install -r /requirements.txt


# copy the content of the local app directory to the working directory
COPY app/ /app
# command to run on container start
CMD [ "python3", "/app/runtime-updater.py" ]

RUN [ "cross-build-end" ]

# ref. https://community.home-assistant.io/t/migration-to-2021-7-fails-fatal-python-error-init-interp-main-cant-initialize-time/320648/10
