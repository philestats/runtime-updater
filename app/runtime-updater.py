from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import sys
import logging
import json
import shutil
import yaml
from yaml.loader import SafeLoader
import paho.mqtt.client as mqtt

"""
The event pushed by 
{"push_data": {"pushed_at": 1633210820, "images": [], "tag": "prod-3.0.0", "pusher": "name_of_the_pusher"}, "callback_url": "https://registry.hub.docker.com/u/docker-user/docker-repo/hook/xxxxx/", "repository": {"status": "Active", "description": "", "is_trusted": false, "full_description": "", "repo_url": "https://hub.docker.com/r/docker-user/docker-repo", "owner": "docker-user", "is_official": false, "is_private": false, "name": "airzone", "namespace": "docker-user", "star_count": 0, "comment_count": 0, "date_created": 1632726141, "repo_name": "docker-user/docker-repo"}}
                                                              ----------
"""
class S(BaseHTTPRequestHandler):
    def update_docker_compose(self, post_data):
        json_data = json.loads(post_data)
        # image name and new tag are parsed from the post data
        image_new = json_data["repository"]["repo_name"]
        tag_new = json_data["push_data"]["tag"]
        logging.info(f"New: {image_new}:{tag_new}")
        # load the original docker-compose.yaml
        dockercompose = None
        with open(docker_compose_file) as f:
            dockercompose = yaml.load(f, Loader=SafeLoader)
        if dockercompose is not None:
            logging.info("File docker-compose.yaml successfully loaded")
            # seek the service to update. Ba careful that not all service image have an explicit tag
            for service in dockercompose["services"]:
                if image_new in dockercompose["services"][service]["image"]:
                    image_old = dockercompose["services"][service]["image"].split(':')[0]
                    tag_old = dockercompose["services"][service]["image"].split(':')[1]
                    if image_old == image_new:
                        # found the entry to update
                        logging.info(f"replace: {image_old}:{tag_old} by {image_new}:{tag_new}")
                        dockercompose["services"][service]["image"] = f"{image_new}:{tag_new}"
                        # backup the original docker-compose to docker-compose.yaml.bkup
                        shutil.copy(docker_compose_file, docker_compose_backup_file)

                        with open(docker_compose_file, 'w') as f:
                            yaml.dump(dockercompose, f, sort_keys=False, default_flow_style=False)
                            logging.info(f"docker-compose.yaml file updated")

                        update_image_notification = {"channel": "update-image",
                                                          "msg": {"agent": "Runtime Updater",
                                                                  "status": "Update image",
                                                                  "service": image_new,
                                                                  "old_tag": tag_old,
                                                                  "new_tag": tag_new
                                                                  }
                                                        }
                        logger.info(update_image_notification)
                        mqtt_client_n = mqtt.Client()
                        mqtt_client_n.connect(mqtt_host, mqtt_port, 60)
                        mqtt_client_n.publish(mqtt_notification_topic, json.dumps(update_image_notification))
                        mqtt_client_n.disconnect()

                        cmd = f"ssh {user_host}@{ip_host} {dc_cmd} -f {dc_path_host}/{dc_name} up -d"
                        logging.info(f"Now run command: {cmd}")
                        os.system(cmd)
                        # todo
                        # check if docker compose failed, revert the backup and start it again

    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                str(self.path), str(self.headers), post_data.decode('utf-8'))

        self.update_docker_compose(post_data)

        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s — %(name)s — %(levelname)s — %(funcName)s:%(lineno)d — %(message)s')
    logger = logging.getLogger(__name__)

    all_ok = True
    # internal TCP listening port for http service
    service_port = os.getenv("RUNTIME_UPDATER_PORT")
    if not service_port:
        logging.critical("RUNTIME_UPDATER_PORT env var not found")
        all_ok = False
    else:
        service_port = int(service_port)
        logging.debug(f"RUNTIME_UPDATER_PORT={service_port}")

    # the name of the docker-compose file, should be like docker-compose.yaml
    dc_name = os.getenv("DOCKERCOMPOSE_FILE_NAME")
    if not dc_name:
        logging.critical("DOCKERCOMPOSE_FILE_NAME env var not found")
        all_ok = False
    else:
        logging.debug(f"DOCKERCOMPOSE_FILE_NAME={dc_name}")

    # path of the docker compose file on the container
    dc_path = os.getenv("DOCKERCOMPOSE_FILE_PATH")
    if not dc_path:
        dc_path="."
    logging.debug(f"DOCKERCOMPOSE_FILE_PATH={dc_path}")

    # path of the docker compose file on the host
    dc_path_host = os.getenv("DOCKERCOMPOSE_FILE_PATH_HOST")
    if not dc_path_host:
        logging.critical("DOCKERCOMPOSE_FILE_PATH_HOST env var not found")
        all_ok = False
    else:
        logging.debug(f"DOCKERCOMPOSE_FILE_PATH_HOST={dc_path_host}")

    # user on the host. It will be used to execute a command from the container to the host via ssh
    user_host = os.getenv("USER_HOST")
    if not user_host:
        logging.critical("USER_HOST env var not found")
        all_ok = False
    else:
        logging.debug(f"USER_HOST={user_host}")

    # Ip address of the host: will be used to execute ssh command
    ip_host = os.getenv("IP_HOST")
    if not ip_host:
        logging.critical("IP_HOST env var not found")
        all_ok = False
    else:
        logging.debug(f"IP_HOST={ip_host}")

    # full path docker-compose command on the host
    dc_cmd = os.getenv("DOCKERCOMPOSE_CMD")
    if not dc_cmd:
        logging.critical("DOCKERCOMPOSE_CMD env var not found")
        all_ok = False
    else:
        logging.debug(f"DOCKERCOMPOSE_CMD={dc_cmd}")

    mqtt_host = os.getenv("MQTT_HOST")
    if not mqtt_host:
        logging.critical("MQTT_HOST env var not found")
        all_ok = False
    else:
        logging.debug(f"MQTT_HOST={mqtt_host}")

    mqtt_port = int(os.getenv("MQTT_PORT"))
    if not mqtt_port:
        logging.critical("MQTT_PORT env var not found")
        all_ok = False
    else:
        mqtt_port = int(mqtt_port)
        logging.debug(f"MQTT_PORT={mqtt_port}")

    mqtt_notification_topic = os.getenv("TOPIC_NOTIFICATION")
    if not mqtt_notification_topic:
        logging.critical("TOPIC_NOTIFICATION env var not found")
        all_ok = False
    else:
        logging.debug(f"TOPIC_NOTIFICATION={mqtt_notification_topic}")

    if not all_ok:
        logger.critical("Some missing envar are missing, the program will not start")
        if mqtt_notification_topic and mqtt_host and mqtt_port is not None:
            error_notification = {"channel": "hermes-status",
                                  "msg": {"agent": "Runtime Updater",
                                          "status": "error",
                                          "data": "Some envvars missing"
                                          }
                                  }
            mqtt_client = mqtt.Client()
            mqtt_client.connect(mqtt_host, mqtt_port, 60)
            mqtt_client.publish(mqtt_notification_topic, json.dumps(error_notification))
            mqtt_client.disconnect()
        sys.exit(1)

    startup_notification = {"channel": "hermes-status",
                          "msg": {"agent": "Runtime Updater",
                                  "status": "OK",
                                  "data": "Starting up"
                                  }
                          }
    mqtt_client = mqtt.Client()
    mqtt_client.connect(mqtt_host, mqtt_port, 60)
    mqtt_client.publish(mqtt_notification_topic, json.dumps(startup_notification))
    mqtt_client.disconnect()

    docker_compose_file=f"{dc_path}/{dc_name}"
    docker_compose_backup_file=f"{dc_path}/{dc_name}.backup"
    logging.debug(f"docker_compose={docker_compose_file}")
    logging.debug(f"docker_compose_backup_file={docker_compose_backup_file}")
    run(port=service_port)
