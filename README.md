# README #

This repo hosts a mecanism used to update the tag value of a service defined in a docker-compose.yaml file from a dockerhub webhook
It works using a relay from a docker hub to an agent running on a host non accessible from the internet
Several deployment cases:
1. the host running the docker compose is accessible from the internet (at least the port of runtime-updater)
bitbucket -> pipeline build docker image -> push to dockerhub -> webhook -> runtime updater
2. the host running the docker compose is not accessible from the internet, we setup a relay with a local agent
bitbucket -> pipeline build docker image -> push to dockerhub -> webhook -> webhookrelay.com tunnel -> local webhookrelay agent -> runtime updater


1. Setup a service whose source is in a bitbucket git repository
2. Add a build pipeline that builds the docker image and pushes it to a dockerhub repository.
3. Use an account in a webhook relay service like https://webhookrelay.com/ and create a tunnel. The tunnel relays to a local
4. In the dockerhub repository, add a webhook invoking the tunnel in webhookrelay.com
5. If the host running the docker compose is not accessible from outside, install a local agent (https://docs.webhookrelay.com/installation-options/installation-options/install-cli) providing the key and secret
6. Install the runtime-updater

When the payload from dockerhub is sent (json POST data), the webrelay just forwards it to it tunnel
The local agent subscribes the tunnel as a websocket and forwards the incoming messages as a POST request to the runtime-updater

The runtime updater then gets the initial payload sent by the docker repository
It extracts the new tag of the service to update and writes it down to the docker-compose file
Finally it sends to the host throw ssh a command docker-compose up to reload the stack with the updated tag

mod in service source -> push to bitbucket
                      -> pipeline builds the docker image
                      -> pipeline pushes the image to dockerhub
                      -> dockerhub gets the new tag for the image
                      -> dockerhub sends a webhook (payload with the new docker tag)
                      -> webhookrelay.com tunnel activated
                      -> websocket (payload with the new docker tag)
                      -> local webhookrelay agent (payload with the new docker tag)
                      -> runtime-updater gets the payload
                      -> create a backup of the original docker-compose.yaml
                      -> update the docker-compose.yaml with the tag extracted from the payload for the given service
                      -> send a ssh command to the host, docker-compose up -d using the modified docker-compose.yaml

Deployment of runtime:
  - runtime-updater shell have a direct access to the docker-compose file
  - mount the folder containing the docker-compose.yaml file in the container
  - when receiving a post request, the name of the image is extracted from the post payload at: json_data["repository"]["repo_name"]
        the tag is extracted from: json_data["push_data"]["tag"]
  - runtime-updater loads the docker-compose file from the path where it is mounted inside the container
      and replace the tag for the service corresponding to the image with the passed tag

  ex: if json_data["repository"]["repo_name"]  = my_awesome_service and json_data["push_data"]["tag"] = 1.2.3, runtime-updater will modify
  the tag in the service my_awesome_service

  my_awesome_service:
     image: my_awesome_service:1.2.2
   will be replaced by
  my_awesome_service:
     image: my_awesome_service:1.2.3

  Then a command docker-compose up -d will be executed on the host via ssh

 runtime-updater:
    container_name: runtime-updater
    env_file:
    - ../conf/runtime-updater/runtime-updater.conf
    volumes:
    - /path/to/the/current/docker-compose.yaml:/host
    ports:
    - 1234:1234
    image: xxxx/runtime-updater:xxx-0.1.4
    restart: always

Envvar to provide to the runtime updater container
# internal TCP listening port for http service
RUNTIME_UPDATER_PORT=1234
# the name of the docker-compose file, should be like docker-compose.yaml
DOCKERCOMPOSE_FILE_NAME=docker-compose.yaml
# path of the docker compose file on the container = the path where is mounted the host folder
DOCKERCOMPOSE_FILE_PATH=/host
# user on the host. It will be used to execute a command from the container to the host via ssh
USER_HOST=xx
# Ip address of the host: will be used to execute ssh command
IP_HOST=192.168.xx.xx
# full path docker-compose command on the host
DOCKERCOMPOSE_CMD=/usr/local/bin/docker-compose
# path of the docker compose file on the host: will be used by the ssh command
DOCKERCOMPOSE_FILE_PATH_HOST=/home/xxx/xxx/bin
# mqtt server and topic used to send notifications
MQTT_HOST=192.168.xx.xx
MQTT_PORT=1883
TOPIC_NOTIFICATION=system/notifications


Howto generate ssh keys

ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/pi/.ssh/id_rsa): ../conf/runtime-updater/ssh/id_rsa





